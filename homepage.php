<?php

/*

	Template Name: Home

*/

get_header(); ?>

	<section id="hero">
	
		<?php if(have_rows('hero_slides')): while(have_rows('hero_slides')): the_row(); ?>
		 
		    <div class="slide">
				<div class="content">

					<div class="photo">
						<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					</div>

					<div class="caption">
						<div class="caption-wrapper">
							<p><?php the_sub_field('caption'); ?></p>
						</div>				
					</div>	

				</div>	
		    </div>

		<?php endwhile; endif; ?>

	</section>

	<section id="about-statement">
		<div class="wrapper">

			<p><?php the_field('about_statement'); ?></p>

		</div>
	</section>


	<section id="case-studies">
		<div class="wrapper">

			<div class="section-header-center">
				<h3 class="section-header">Case Studies</h3>
			</div>	

			<?php
				$args = array(
					'post_type' => 'projects',
					'posts_per_page' => 6,
			        'meta_query' => array(
			            array(
			                'key' => '_wp_page_template',
			                'value' => 'case-study.php',
			                'compare' => '='
			            )
			        )
				);
				$case_studies = new WP_Query( $args );
				if ( $case_studies->have_posts() ) : ?>

					<div class="project-list">
						<?php while ( $case_studies->have_posts() ) : $case_studies->the_post(); ?>
							<?php get_template_part('partials/project'); ?>
						<?php endwhile; ?>
					</div>

			<?php endif; wp_reset_postdata(); ?>

		</div>
	</section>


	<section id="our-firm">
		<div class="wrapper">
			<img src="<?php $image = get_field('our_firm_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

			<div class="inset">
				<div class="wrapper">
					
					<div class="info">
						<div class="copy">
							<h3>Our Firm</h3>
							<p><?php the_field('our_firm_caption'); ?></p>
						</div>

						<div class="cta">
							<a href="<?php echo site_url('/about/'); ?>" class="learn-more">Learn more</a>						
						</div>	
					</div>

				</div>
			</div>			

		</div>
	</section>


	<section id="blog">
		<div class="wrapper">

			<div class="section-header-center">
				<h3 class="section-header">Blog</h3>
			</div>	

			<div class="posts">

				<?php
					$args = array(
						'post_type' => 'post',
						'posts_per_page' => 3
					);
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post(); ?>

						<?php get_template_part('partials/blog-preview'); ?>

				<?php endwhile; endif; wp_reset_postdata(); ?>

			</div>

		</div>
	</section>

<?php get_footer(); ?>