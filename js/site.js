new bilderrahmen({
	closeOnOutsideClick: false
});

$(document).ready(function() {

	// rel="external"
	$('a[rel="external"]').click( function() {
		window.open( $(this).attr('href') );
		return false;
	});

	// Menu Toggle
	$('#toggle').click(function(){
		$('header, nav').toggleClass('open');
		return false;
	});

	// Home Hero Carousel
	$('body.home #hero').slick({
		dots: true,
		arrows: false,
		infinite: true,
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 6000,
	});



	// Service Filter
	$('.service .filter-btn').on('click', function(){

		if($(this).hasClass('active')) {

			$('.filter.service').attr('data-filter', '');
			$('.service .filter-btn').removeClass('active');

		} else {

			$('.service .filter-btn').removeClass('active');
			$(this).addClass('active');

			var dataID = $(this).data('id');
			$('.filter.service').attr('data-filter', dataID);
		}


		return false;

	});


	// Phase Filter
	$('.phase .filter-btn').on('click', function(){

		if($(this).hasClass('active')) {

			$('.filter.phase').attr('data-filter', '');
			$('.phase .filter-btn').removeClass('active');

		} else {

			$('.phase .filter-btn').removeClass('active');
			$(this).addClass('active');

			var dataID = $(this).data('id');
			$('.filter.phase').attr('data-filter', dataID);
		}


		return false;

	});


	// Filter Toggle
	$('.filters h3 a').on('click', function(){
		$(this).toggleClass('active');

		$('.filter-wrapper').slideToggle(200);
		return false;

	});


	// Before and After Toggle
	$('.before-after a').on('click', function(){
		$('.before-after a').removeClass('active');
		$(this).addClass('active');

		return false;

	});


	// Client List Toggle
	$('.client-list .toggle .show').on('click', function(){
		var list = $(this).attr('href');
		$(list).toggleClass('hide');

		$(this).text(function(i, text){
		    return text === "[Show]" ? "[Hide]" : "[Show]";
		})

		return false;
	});


	// Search Trigger
	$('.consult-trigger').click(function(){
		$('body').toggleClass('consult-open');
		return false;
	});

	// Search Close
	$('.consult-close').click(function(){
		$('body').toggleClass('consult-open');
		return false;
	});

		
});

$(document).keyup(function(e) {
	
	if (e.keyCode == 27) {
		$('body').removeClass('consult-open');
	}

});