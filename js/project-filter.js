jQuery(function($) {

	// Load posts on page load
	get_projects();

	// Get Selected Year
	function getSelectedYear() {
		var year = $('.filter.year select').val();	

		if(!year == '') {
			return year;
		}
	}

	// Get Selected Comparison
	function getSelectedComparison() {
		var comparison = $('.filter.year .before-after a.active').data('time');	

		return comparison;
	}

	// Get Selected Service
	function getSelectedService() {
		var service = $('.filter.service').attr('data-filter');	

		return service;
	}

	// Get Selected Phase
	function getSelectedPhase() {
		var phase = $('.filter.phase').attr('data-filter');	
	
		return phase;
	}

	// Get Selected Location
	function getSelectedLocation() {
		var location = $('.filter.location select').val();	

		if(!location == '') {
			return location;
		}
	}

	// On select filter change
	$('.filter select').on('change', function(){
		get_projects();
	});

	// On select filter button click
	$('.filter-btn, .before-after a').on('click', function(){
		get_projects();
	});
	
	//Main ajax function
	function get_projects() {
		var ajax_url = ajax_listing_params.ajax_url;

		$.ajax({
			type: 'GET',
			url: ajax_url,
			data: {
				action: 'project_filter',
				year: getSelectedYear,
				comparison: getSelectedComparison,
				service: getSelectedService,
				phase: getSelectedPhase,
				location: getSelectedLocation
			},
			beforeSend: function () {
				$('body.post-type-archive-projects #response').html();
				
			},
			success: function(data) {
				//Hide loader here
				$('body.post-type-archive-projects #response').html(data);


			},
			error: function() {
				$("body.post-type-archive-projects #response").html('<p>There has been an error</p>');
			}
		});				
	}
	
});