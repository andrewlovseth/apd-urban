<?php get_header(); ?>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_photo'); echo $image['url']; ?>);">
		<div class="wrapper">


		</div>
	</section>

	<section id="page-header">
		<div class="wrapper">

			<h1 class="page-header"><?php the_title(); ?></h1>
			<h2><?php the_field('location'); ?></h2>
	
		</div>
	</section>

	<section id="content">
		<div class="wrapper">

			<article>					
				<?php if(get_field('project_scope')): ?>
					<section id="project-scope" class="copy">
						<h3 class="section-header">Project Scope</h3>
						<?php the_field('project_scope'); ?>
					</section>
				<?php endif; ?>

				<?php if(get_field('our_work')): ?>
					<section id="our-work" class="copy">
						<h3 class="section-header">Our Work</h3>
						<?php the_field('our_work'); ?>
					</section>
				<?php endif; ?>

				<?php if(get_field('deliverables')): ?>
					<section id="deliverables" class="copy">
						<h3 class="section-header">Deliverables</h3>
						<?php the_field('deliverables'); ?>
					</section>
				<?php endif; ?>

				<?php $images = get_field('gallery'); if( $images ): ?>
					<div id="gallery">
						<div class="header">
							<h3 class="section-header">Gallery</h3>
						</div>

						<?php foreach( $images as $image ): ?>
							<div class="image">
								<a href="<?php echo $image['sizes']['large']; ?>">
									<img data-bilderrahmen="gallery-01" src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
							</div>
						<?php endforeach; ?>
					</div>
				<?php endif; ?>

			</article>

			<aside>

				<div id="details">
					<?php if(get_field('client')): ?>
						<div class="fact">
							<h4>Client</h4>
							<p><?php the_field('client'); ?></p>
						</div>
					<?php endif; ?>

					<?php if(get_field('start_year')): ?>
						<div class="fact date-of-service">
							<h4>Years of Service</h4>
							<p>
								<?php the_field('start_year'); ?>

								<?php if(get_field('end_year') && get_field('start_year') != get_field('end_year') ): ?>
									&mdash; <?php the_field('end_year'); ?>
								<?php endif; ?>
							</p>
						</div>
					<?php endif; ?>

					<?php if(get_field('key_facts')): ?>
						<div class="fact key-facts">
							<h4>Key Facts</h4>
							<?php the_field('key_facts'); ?>
						</div>
					<?php endif; ?>

					<?php $services = get_field('services'); if($services):  ?>
						<div class="fact services">
							<h4>Services</h4>
							<ul>
								<?php foreach( $services as $service ): ?>
									<li class="<?php echo sanitize_title_with_dashes(get_the_title( $service->ID )); ?>"><a href="<?php echo get_permalink($service->ID); ?>"><?php echo get_the_title( $service->ID ); ?></a></li>
								<?php endforeach; ?>
							</ul>
						</div>
					<?php endif; ?>


					<?php $skills = get_field('skills'); if($skills): ?>
						<div class="fact skills">
							<h4>Skills</h4>
							<div class="skills-wrapper">
							    <?php foreach( $skills as $skill): ?>
							        <a href="<?php echo get_permalink($skill->ID); ?>"><?php echo get_the_title($skill->ID); ?></a>
							    <?php endforeach; ?>
							</div>
						</div>
					<?php endif; ?>
				</div>

			</aside>
			

		</div>
	</section>

	<?php $related_projects = get_field('related_projects'); if( $related_projects ): ?>

		<section id="related-projects">
			<div class="wrapper">

				<h3 class="section-header">Related Projects</h3>

				<div class="project-list">
				
					<?php foreach( $related_projects as $related_project ): ?>		

						<div class="project">
							<div class="photo">
								<a href="<?php echo get_permalink($related_project->ID); ?>" class="cover" style="background-image: url(<?php $image = get_field('hero_photo', $related_project->ID); echo $image['sizes']['medium']; ?>);">
								</a>
							</div>
							<div class="info">
								<h4><a href="<?php echo get_permalink($related_project->ID); ?>"><?php echo get_the_title($related_project->ID); ?></a></h4>
								<h5><?php the_field('location', $related_project->ID); ?></h5>
							</div>
						</div>
						
					<?php endforeach; ?>
		
				</div>

			</div>
		</section>

	<?php endif; ?>



<?php get_footer(); ?>