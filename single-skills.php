<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">
			
			<div class="header center">
				<h1 class="page-header"><?php the_title(); ?></h1>
			</div>

			<div class="description">
				<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>

			<?php
				$args = array(
					'post_type' => 'projects',
					'posts_per_page' => 200,
			        'meta_query' => array(
			            array(
			                'key' => '_wp_page_template',
			                'value' => 'case-study.php',
			                'compare' => '!='
			            ),
						array(
							'key' => 'skills',
							'value' => '"' . get_the_ID() . '"',
							'compare' => 'LIKE'
						)
			        )
				);
				$projects = new WP_Query( $args );
				if ( $projects->have_posts() ) : ?>

					<div class="project-list">
						<?php while ( $projects->have_posts() ) : $projects->the_post(); ?>
							<?php get_template_part('partials/project'); ?>
						<?php endwhile; ?>
					</div>

				<?php endif; wp_reset_postdata(); ?>


	
		</div>
	</section>





<?php get_footer(); ?>