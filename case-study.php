<?php

/*

	Template Name: Case Study
	Template Post Type: projects

*/

get_header(); ?>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('hero_photo'); echo $image['url']; ?>);">
		<div class="wrapper">


		</div>
	</section>

	<section id="page-header">
		<div class="wrapper">

			<h3>Case Study</h3>
			<h1 class="page-header"><?php the_title(); ?></h1>
			<h2><?php the_field('location'); ?></h2>
	
		</div>
	</section>


	<section id="prologue">
		<div class="wrapper">

			<div class="section-header-center">
				<h3 class="section-header">Prologue</h3>
			</div>	

			<div class="image">
				<img src="<?php $image = get_field('prologue_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="info">
				<?php the_field('prologue'); ?>
			</div>

		</div>
	</section>

	<section class="case-study">

		<?php if(have_rows('sections')): while(have_rows('sections')) : the_row(); ?>

		    <?php if( get_row_layout() == 'section_header' ): ?>
				
				<section class="section-header-center">
					<h3 class="section-header"><?php the_sub_field('headline'); ?></h3>
				</section>	
				
		    <?php endif; ?>
		 

		    <?php if( get_row_layout() == 'text' ): ?>
				
				<section class="text">
					<div class="wrapper">

						<div class="info">
						
							<?php if(get_sub_field('headline')): ?>
								<h3><?php the_sub_field('headline'); ?></h3>
							<?php endif; ?>

							<?php if(get_sub_field('copy')): ?>
								<?php the_sub_field('copy'); ?>	    		
							<?php endif; ?>
	
						</div>
				
					</div>
				</section>
				
		    <?php endif; ?>
		 

		    <?php if( get_row_layout() == 'side_by_side' ): ?>

		    	<?php $info = get_sub_field('info'); ?>
				
				<section class="side-by-side <?php echo $info['image_align'] ?>">
					<div class="wrapper">

						<div class="info">
							<div class="info-wrapper">
								<h3><?php echo $info['headline'] ?></h3>
								<?php echo $info['copy'] ?>
							</div>
						</div>

						<?php if(get_sub_field('image')): ?>
							<div class="image">
								<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							</div>
						<?php endif; ?>
				
					</div>
				</section>
				
		    <?php endif; ?>

		 
		    <?php if( get_row_layout() == 'two_column' ): ?>
				
				<section class="two-column">
					<div class="wrapper">

						<?php if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>
							 
						    <?php get_template_part('partials/case-study-col'); ?>		

						<?php endwhile; endif; ?>
						
					</div>
				</section>
				
		    <?php endif; ?>


		    <?php if( get_row_layout() == 'three_column' ): ?>
				
				<section class="three-column">
					<div class="wrapper">

						<?php if(have_rows('columns')): while(have_rows('columns')): the_row(); ?>

							<?php get_template_part('partials/case-study-col'); ?>						 

						<?php endwhile; endif; ?>
						
					</div>
				</section>
				
		    <?php endif; ?>
		 

		    <?php if( get_row_layout() == 'full_width_inset' ): ?>
				<section class="full-width-inset">
					<div class="content cover" style="background-image: url(<?php $image = get_sub_field('image'); echo $image['url']; ?>)">

						<div class="wrapper">
							
							<?php if(get_sub_field('headline')): ?>
								<div class="inset desktop">
									<h3><?php the_sub_field('headline'); ?></h3>

									<?php if(get_sub_field('copy')): ?>
										<?php the_sub_field('copy'); ?>
									<?php endif; ?>
								</div>

							<?php endif; ?>

						</div>

					</div>
				</section>

				<div class="inset mobile">
					<?php if(get_sub_field('headline')): ?>
						<h3><?php the_sub_field('headline'); ?></h3>
					<?php endif; ?>

					<?php if(get_sub_field('headline')): ?>
						<?php the_sub_field('copy'); ?>
					<?php endif; ?>
				</div>
				
		    <?php endif; ?>


		<?php endwhile; endif; ?>
	</section>

	<section id="epilogue">
		<div class="wrapper">

			<div class="section-header-center">
				<h3 class="section-header">Epilogue</h3>
			</div>	

			<div class="image">
				<img src="<?php $image = get_field('epilogue_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</div>

			<div class="info">
				<?php the_field('epilogue'); ?>
			</div>

		</div>
	</section>

<?php get_footer(); ?>