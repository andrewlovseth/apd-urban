<section class="consult-overlay">
	<div class="overlay">
		<div class="overlay-wrapper">

				<div class="close">
					<a href="#" class="close-btn consult-close"></a>
				</div>

			<div class="info">
				<div class="form">
					<?php echo do_shortcode('[wpforms id="1377" title="true" description="true"]'); ?>
				</div>
			</div>
			
		</div>
	</div>
</section>
