<?php

/*

	Template Name: Press

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>
	
	<section id="main">
		<div class="wrapper">
			
			<div class="header center">
				<h1 class="page-header"><?php the_title(); ?></h1>
			</div>

			<section class="press-copy">
				<?php the_field('press_copy'); ?>
			</section>

			<section class="press-clippings">
				<?php if(have_rows('press_clippings')): while(have_rows('press_clippings')): the_row(); ?>
				 
				    <div class="press-clip">
				    	<a href="<?php the_sub_field('link'); ?>" rel="extenral">
					    	<span class="publication">
					    		<?php the_sub_field('publication'); ?>
					    	</span>

					    	<span class="article">
					    		<?php the_sub_field('article'); ?>
					    	</span>

				    		<span class="date">&mdash;  <?php the_sub_field('date'); ?></span>
				    	</a>				        
				    </div>

				<?php endwhile; endif; ?>
			</section>

		</div>
	</section>

<?php get_footer(); ?>