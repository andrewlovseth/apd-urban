<div class="col col-2 nav">
	<div class="nav">
		<a href="<?php echo site_url('/'); ?>">Home</a>

		<?php if(have_rows('our_work_nav', 'options')): while(have_rows('our_work_nav', 'options')): the_row(); ?>
		     <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
		<?php endwhile; endif; ?>


		<?php if(have_rows('about_us_nav', 'options')): while(have_rows('about_us_nav', 'options')): the_row(); ?>
		     <a href="<?php the_sub_field('link'); ?>"><?php the_sub_field('label'); ?></a>
		<?php endwhile; endif; ?>
	</div>
</div>