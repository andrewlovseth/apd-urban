<div class="col">
	<div class="image">
		<img src="<?php $image = get_sub_field('image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
	</div>

	<div class="info">
		<h3><?php the_sub_field('headline'); ?></h3>
		<?php the_sub_field('copy'); ?>
	</div>    		
</div>