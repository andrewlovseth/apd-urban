<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">

			<section id="projects">
					
				<div class="header center">
					<h1 class="page-header">Projects</h1>
					<h2 class="subheadline"><?php the_field('projects_subheadline', 'options'); ?></h2>

					<div class="copy">
						<?php the_field('projects_copy', 'options'); ?>
					</div>					
				</div>

				<div class="filters">

					<div class="header">
						<h3><a href="#">Filters <span>+</span></a></h3>
					</div>

					<div class="filter-wrapper">
						<div class="flex-wrapper">

							<div class="year filter">
								<h4>Date</h4>

								<div class="before-after">
									<a href="#" class="before" data-time="<=">Before</a>
									<a href="#" class="after active" data-time=">=">After</a>
								</div>								

								<select>
									<option value="">&mdash; Any Date &mdash;</option>
									<?php
										$years_field = get_field_object('start_year', 46);
										$years = $years_field['choices'];
										foreach($years as $year): ?>

											<option value="<?php echo $year; ?>"><?php echo $year; ?></option>

									<?php endforeach; ?>
								</select>	
							</div>


							<div class="service filter" data-filter="">
								<h4>Services</h4>
								
								<?php
									$args = array(
										'post_type' => 'services',
										'posts_per_page' => 25,
										'order' => 'ASC',
										'orderby' => 'title',
										'post__not_in' => array(270, 275)
									);
									$services_list = new WP_Query( $args );
									if($services_list->have_posts()) : ?>
									
										<div class="service-list">

											<?php while ( $services_list->have_posts() ) : $services_list->the_post(); ?>

												<a href="#" class="filter-btn" data-id="<?php echo $post->ID; ?>"><?php the_title(); ?></a>
											
											<?php endwhile; ?>
											
										</div>

								<?php endif; wp_reset_postdata(); ?>

							</div>					


							<div class="phase filter" data-filter="">
								<h4>Phase</h4>
								<a href="#" class="filter-btn" data-id="270">Analysis + Strategy</a>
								<a href="#" class="filter-btn" data-id="275">Pre-Development + Implementation</a>						
							</div>		


							<div class="location filter">
								<h4>Location</h4>
			
								<select>
									<option value="">&mdash; Any Location &mdash;</option>
									<?php 
										$states_field = get_field_object('state', 46);
										$states = $states_field['choices'];
										foreach($states as $state): ?>

											<option value="<?php echo sanitize_title_with_dashes($state); ?>"><?php echo $state; ?></option>

									<?php endforeach; ?>
								</select>	
							</div>
							
						</div>
					</div>

				</div>

				<section id="response" class="project-list"></section>

			</section>

		</div>
	</section>
	
<?php get_footer(); ?>