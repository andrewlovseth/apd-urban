<?php

/*

	Template Name: Clients

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>
	
	<section id="main">
		<div class="wrapper">
			
			<div class="header center">
				<h1 class="page-header">Our Clients</h1>
			</div>

			<div class="copy">
				<?php the_field('copy'); ?>
			</div>

			<?php if(have_rows('clients')): $index = 1; while(have_rows('clients')) : the_row(); ?>
			 
			    <?php if( get_row_layout() == 'section' ): ?>
					
					<section class="client-list">
						<div class="header center">
							<h3 class="section-header"><?php the_sub_field('label'); ?></h3>
						</div>

						<div class="toggle">
							<a href="#list-<?php echo $index; ?>" class="show">[Show]</a>
						</div>


						<?php if(have_rows('list')): ?>

							<div id="list-<?php echo $index; ?>" class="list hide">
								
								<?php while(have_rows('list')): the_row(); ?>
				 
								    <div class="client">
								    	<?php if(get_sub_field('logo')): ?>
											<div class="logo">
												<img src="<?php $image = get_sub_field('logo'); echo $image['sizes']['medium']; ?>" alt="<?php echo $image['alt']; ?>" />
											</div>
									    <?php endif; ?>
								        <h4><?php the_sub_field('name'); ?></h4>
								    </div>

								<?php endwhile; ?>

							</div>

						<?php endif; ?>

					</section>
					
			    <?php endif; ?>
			 
			<?php $index++; endwhile; endif; ?>


			<section id="where-we-work">
				<div class="header center">
					<h3 class="section-header"><?php the_field('where_we_work_headline'); ?></h3>
				</div>

				<div class="copy">
					<?php the_field('where_we_work_copy'); ?>
				</div>

				<div class="image">
					<img src="<?php $image = get_field('where_we_work_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>


			</section>

		</div>
	</section>

<?php get_footer(); ?>