<?php

/*

	Template Name: Case Studies

*/

get_header(); ?>

	<section id="main">
		<div class="wrapper">

			<section id="projects">
					
				<div class="header center">
					<h1 class="page-header">Case Studies</h1>

					<div class="copy">
						<?php the_field('case_studies_copy'); ?>
					</div>					
				</div>

				<?php
					$args = array(
						'post_type' => 'projects',
						'posts_per_page' => 6,
				        'meta_query' => array(
				            array(
				                'key' => '_wp_page_template',
				                'value' => 'case-study.php',
				                'compare' => '='
				            )
				        )
					);
					$case_studies = new WP_Query( $args );
					if ( $case_studies->have_posts() ) : ?>

						<div class="project-list">
							<?php while ( $case_studies->have_posts() ) : $case_studies->the_post(); ?>
								<?php get_template_part('partials/project'); ?>
							<?php endwhile; ?>
						</div>

				<?php endif; wp_reset_postdata(); ?>

			</section>

		</div>
	</section>
	
<?php get_footer(); ?>