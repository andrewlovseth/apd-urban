<?php

/*

	Template Name: About

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>
	
	<section id="main">
		<div class="wrapper">
			
			<div class="header center">
				<h1 class="page-header">Our Firm</h1>
			</div>

			<section id="about-copy">
				<?php the_field('about_copy'); ?>
			</section>

			<section id="mission-statement">
				<p><?php the_field('mission_statement', 'options'); ?></p>
			</section>

			<section id="team">
				<div class="header center">
					<h3 class="section-header">Our Team</h3>
				</div>

				<?php $posts = get_field('people'); if( $posts ): ?>

					<div class="people-list">

					    <?php foreach( $posts as $post): setup_postdata($post); ?>

					        <div class="people">
					        	<div class="photo">
					        		<a href="<?php the_permalink(); ?>">
					        			<img src="<?php $image = get_field('headshot'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
					        		</a>
					        	</div>

					        	<div class="info">
					        		<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
					        		<h5><?php the_field('title'); ?></h5>
					        	</div>
					        
					        </div>

					    <?php endforeach; ?>

					</div>

				<?php wp_reset_postdata(); endif; ?>
			</section>

		</div>
	</section>

<?php get_footer(); ?>