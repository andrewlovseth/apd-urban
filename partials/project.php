<div class="project">
	<div class="photo">
		<a href="<?php the_permalink(); ?>" class="cover" style="background-image: url(<?php $image = get_field('hero_photo'); echo $image['sizes']['medium']; ?>);">
		</a>
	</div>
	<div class="info">
		<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
		<h5><?php the_field('location'); ?></h5>
	</div>
</div>