<a href="#" id="toggle">
	<div class="patty"></div>
</a>

<section class="navigation">
	<nav>
		<div class="subnav home">
			<a href="<?php echo site_url('/'); ?>" class='nav-home'>Home</a>

		</div>

		<div class="subnav">
			<h4>Our Work</h4>

			<?php if(have_rows('our_work_nav', 'options')): while(have_rows('our_work_nav', 'options')): the_row(); ?>
			     <a href="<?php the_sub_field('link'); ?>" class="nav-<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>"><?php the_sub_field('label'); ?></a>
			<?php endwhile; endif; ?>

		</div>

		<div class="subnav">
			<h4>About Us</h4>

			<?php if(have_rows('about_us_nav', 'options')): while(have_rows('about_us_nav', 'options')): the_row(); ?>
			     <a href="<?php the_sub_field('link'); ?>" class="nav-<?php echo sanitize_title_with_dashes(get_sub_field('label')); ?>"><?php the_sub_field('label'); ?></a>
			<?php endwhile; endif; ?>
		</div>

		<div class="cta mobile-cta">
			<a href="#" class="btn consult-trigger">Free Consult</a>
		</div>
	</nav>

	<div class="tagline">
		<div class="tagline-wrapper">
			<h2><?php the_field('tagline', 'options'); ?></h2>
		</div>

		<div class="cta">
			<a href="#" class="btn consult-trigger">Free Consult</a>
		</div>
		
	</div>
</section>