	<footer>
		<div class="wrapper">
			<div class="col-wrapper">

				<?php get_template_part('partials/footer/site-info'); ?>

				<?php get_template_part('partials/footer/navigation'); ?>

				<?php get_template_part('partials/footer/contact-info'); ?>
				
			</div>
		</div>
	</footer>

	<?php get_template_part('partials/footer/consult-overlay'); ?>
	
	<?php wp_footer(); ?>

	<?php get_template_part('partials/footer/ga'); ?>

</body>
</html>