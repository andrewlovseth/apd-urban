<div class="col col-1 site-info">
	<div class="logo">
		<a href="<?php echo site_url('/'); ?>">
			<img src="<?php $image = get_field('footer_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
		</a>		
	</div>

	<div class="copyright">
		<p><?php the_field('copyright', 'options'); ?></p>
	</div>

</div>