<?php get_header(); ?>

	<section id="page-header">
		<div class="wrapper">

			<h1 class="page-header"><?php the_title(); ?></h1>
			<h2><?php the_field('title'); ?></h2>

			<div class="bio-leader">
				<?php the_field('bio_leader'); ?>
			</div>
	
		</div>
	</section>

	<section id="profile">
		<div class="wrapper">
			
			<aside>
				<div class="photo">
					<img src="<?php $image = get_field('headshot'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>

				<?php if(have_rows('education')): ?>

					<div class="education">
						<h4>Education</h4>

						<?php while(have_rows('education')): the_row(); ?>
					 
						    <div class="degree">
						    	<h3><?php the_sub_field('degree'); ?></h3>
						        <p><?php the_sub_field('school'); ?>, <em><?php the_sub_field('city'); ?></em></p>
						    </div>

						<?php endwhile; ?>

					</div>

				<?php endif; ?>

				<?php if(have_rows('skill_highlights')): ?>

					<div class="skill-highlights">
						<h4>Skill Highlights</h4>

						<?php while(have_rows('skill_highlights')): the_row(); ?>
					 
						    <div class="skill">
						    	<h3><?php the_sub_field('skill'); ?></h3>
						    </div>

						<?php endwhile; ?>

					</div>

				<?php endif; ?>

				<?php if(have_rows('professional_affiliations')): ?>

					<div class="professional-affiliations">
						<h4>Professional Affiliations</h4>

						<?php while(have_rows('professional_affiliations')): the_row(); ?>
					 
						    <div class="affiliation">
						    	<h3><?php the_sub_field('affiliation'); ?></h3>
						    </div>

						<?php endwhile; ?>

					</div>

				<?php endif; ?>
			</aside>

			<article>
				<?php if(get_field('linkedin')): ?>
					<div class="social">
						<h4>Connect</h4>

						<div class="links">
							<a href="<?php the_field('linkedin'); ?>" class="linkedin" rel="external">
								<img src="<?php bloginfo('template_directory'); ?>/images/linkedin-branded.svg" alt="LinkedIn" />
							</a>							
						</div>
					</div>
				<?php endif; ?>

				<div class="bio">
					<h4>Biography</h4>
					<?php the_field('bio'); ?>
				</div>
			</article>


		</div>
	</section>



	<?php $related_projects = get_field('featured_projects'); if( $related_projects ): ?>

		<section id="related-projects">
			<div class="wrapper">

				<h3 class="section-header">Project Highlights</h3>

				<div class="project-list">
				
					<?php foreach( $related_projects as $related_project ): ?>		

						<div class="project">
							<div class="photo">
								<a href="<?php echo get_permalink($related_project->ID); ?>" class="cover" style="background-image: url(<?php $image = get_field('hero_photo', $related_project->ID); echo $image['sizes']['medium']; ?>);">
								</a>
							</div>
							<div class="info">
								<h4><a href="<?php echo get_permalink($related_project->ID); ?>"><?php echo get_the_title($related_project->ID); ?></a></h4>
								<h5><?php the_field('location', $related_project->ID); ?></h5>
							</div>
						</div>
						
					<?php endforeach; ?>
		
				</div>

			</div>
		</section>

	<?php endif; ?>



<?php get_footer(); ?>