<?php
	the_posts_pagination(
		array(
	    'mid_size' => 1,
	    'prev_text' => 'Prev',
	    'next_text' => 'Next'
		)
	);
?>
