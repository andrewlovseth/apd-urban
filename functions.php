<?php

/*

    ----------------------------------------------------------------------
    					XX Theme Support
    ----------------------------------------------------------------------

*/

show_admin_bar( false );

function register_my_menu() {
  register_nav_menu('main-menu',__( 'Main Menu' ));
}
add_action( 'init', 'register_my_menu' );

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

function custom_tiled_gallery_width() {
    return '1200';
}
add_filter( 'tiled_gallery_content_width', 'custom_tiled_gallery_width' );

add_theme_support( 'title-tag' );




/*

    ----------------------------------------------------------------------
    					XX Code Cleanup
    ----------------------------------------------------------------------

*/

remove_action( 'wp_head', 'wp_resource_hints', 2 );
remove_action ('wp_head', 'rsd_link');
remove_action( 'wp_head', 'wlwmanifest_link');
remove_action( 'wp_head', 'wp_shortlink_wp_head');
remove_action( 'wp_head', 'wp_generator');
remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
remove_action( 'wp_head', 'wp_oembed_add_discovery_links', 10 );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );


function remove_image_size_attributes( $html ) {
    return preg_replace( '/(width|height)="\d*"/', '', $html );
}
 
// Remove image size attributes from post thumbnails
add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// Remove image size attributes from images added to a WordPress post
add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );



/* Add all theme styles and scripts */
function add_theme_styles_scripts() {
    wp_enqueue_style( 'site_typefaces', 'https://fonts.googleapis.com/css' . '?family=Libre+Baskerville:400,400i,700|News+Cycle:400,700|Playfair+Display:900i', array(), '1.0.0' );
    wp_enqueue_style( 'bilderrahmen_styles', 'https://cdn.jsdelivr.net/npm/bilderrahmen@0.3.1/dist/bilderrahmen.min.css', array(), '1.0.0' );
    wp_enqueue_style( 'site_styles', get_stylesheet_uri(), array(), '1.0.0' );

    wp_enqueue_script( 'bilderrahmen_script', 'https://cdn.jsdelivr.net/npm/bilderrahmen@0.3.1/dist/bilderrahmen.min.js', array(), '1.0.0', false );    
    wp_enqueue_script( 'site_jquery', 'https://code.jquery.com/jquery-3.2.1.min.js', array(), '3.2.1', false );
    wp_enqueue_script( 'site_plugins', get_stylesheet_directory_uri() . '/js/plugins.js', array(), '1.0.0', true );
    wp_enqueue_script( 'site_scripts', get_stylesheet_directory_uri() . '/js/site.js', array(), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'add_theme_styles_scripts' );



/*

    ----------------------------------------------------------------------
    					XX Custom Functions
    ----------------------------------------------------------------------

*/

/* Filter <p>'s on <img> and <iframe>' */
function filter_ptags_on_images($content) {
	$content = preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	return preg_replace('/<p>\s*(<iframe .*>*.<\/iframe>)\s*<\/p>/iU', '\1', $content);
}
add_filter('the_content', 'filter_ptags_on_images');
add_filter('acf/format_value/type=wysiwyg', 'filter_ptags_on_images', 10, 3);


/* List of Tag Slugs */
function entry_tags() {
	$posttags = get_the_tags();
	if ($posttags) {
	  foreach($posttags as $tag) {
	    echo $tag->slug; 
	  }
	}
}

function replace_admin_menu_icons_css() {
    ?>
    <style>

		.dashicons-admin-post:before,
		.dashicons-format-standard:before {
		    content: "\f331";
		}

    </style>
    <?php
}



function wpdocs_custom_excerpt_length( $length ) {
    return 25;
}
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function wpdocs_excerpt_more( $more ) {
    return '...';
}
add_filter( 'excerpt_more', 'wpdocs_excerpt_more' );






// PROJECT FILTERS

//Enqueue Ajax Scripts
function enqueue_project_filter_ajax_scripts() {
  wp_register_script('project-filter', get_bloginfo('template_url') . '/js/project-filter.js', '', true );
  wp_localize_script('project-filter', 'ajax_listing_params', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
  wp_enqueue_script('project-filter', get_bloginfo('template_url') . '/js/project-filter.js','','',true);
}
add_action('wp_enqueue_scripts', 'enqueue_project_filter_ajax_scripts');

//Add Ajax Actions
add_action('wp_ajax_project_filter', 'ajax_project_filter');
add_action('wp_ajax_nopriv_project_filter', 'ajax_project_filter');


function ajax_project_filter() {

    $query_data = $_GET;
    $year = $query_data['year'];
    $comparison = $query_data['comparison'];
    $location = $query_data['location'];

    // Before && No Date
    if($year == null && $comparison == '<=') {
        $year = '2100'; 
    }

    // After && No Date
    if($year == null && $comparison == '>=') {
        $year = '1970';
    }

    $query_array = array(
        'relation' => 'AND',
        array(
            'key' => 'start_year',
            'value' => $year,
            'compare' => $comparison
        ),
        array(
            'key' => 'state',
            'value' => $location,
            'compare' => 'LIKE'
        ),
        array(
            'key' => '_wp_page_template',
            'value' => 'case-study.php',
            'compare' => '!='
        ),
    );

    if(isset($_GET['service']) && !empty($_GET['service'])){
        $service = $query_data['service'];
        array_push($query_array, 
            array(
                'key' => 'services',
                'value' => '"' . $service . '"',
                'compare' => 'LIKE'
            ) 
        );
    }

    if(isset($_GET['phase']) && !empty($_GET['phase'])){
        $phase = $query_data['phase'];
        array_push($query_array, 
            array(
                'key' => 'services',
                'value' => '"' . $phase . '"',
                'compare' => 'LIKE'
            ) 
        );
    }

    $projects_args = array(
        'post_type' => 'projects',
        'posts_per_page' => 200,
        'post_status' => 'publish',
        'order' => 'ASC',
        'orderby' => 'title',
        'meta_query' =>  $query_array
    );


    $projects_loop = new WP_Query($projects_args);
    if( $projects_loop->have_posts() ): while( $projects_loop->have_posts() ): $projects_loop->the_post();

        get_template_part('partials/project');
    endwhile;   
    else:
        get_template_part('partials/project-none');

    endif;

    wp_reset_postdata();

    die();
}











/*

    ----------------------------------------------------------------------
    					XX Advanced Custom Fields
    ----------------------------------------------------------------------

*/

function my_relationship_query( $args, $field, $post_id ) {
    $args['orderby'] = 'name';
    $args['order'] = 'ASC';
    return $args;
}


if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Site Options');
}


// filter for every field
add_filter('acf/fields/relationship/query', 'my_relationship_query', 10, 3);

function my_acf_admin_head() {
	?>
	<style type="text/css">

		.acf-relationship .list {
			height: 400px;
		}

        @media screen and (min-width: 1200px) {
            .acf-field.two-col .acf-table tbody {
                display: flex;
            }
        }

        @media screen and (min-width: 1440px) {
            .acf-field.three-col .acf-table tbody {
                display: flex;
            }
        }


	</style>

	<?php
}

add_action('acf/input/admin_head', 'my_acf_admin_head');



// Custom Excerpt function for Advanced Custom Fields
function custom_field_excerpt() {
    global $post;
    $text = get_field('description'); //Replace 'your_field_name'
    if ( '' != $text ) {
        $text = strip_shortcodes( $text );
        $text = apply_filters('the_content', $text);
        $text = str_replace(']]&gt;', ']]&gt;', $text);
        $excerpt_length = 10; // 20 words
        $excerpt_more = apply_filters('excerpt_more', ' ' . '...');
        $text = wp_trim_words( $text, $excerpt_length, $excerpt_more );
    }
    return apply_filters('the_excerpt', $text);
}



function related_relationship_query( $args, $field, $post_id ) {
    
    $args['orderby'] = 'name';
    $args['order'] = 'ASC';
    
    
    // return
    return $args;
    
}

// filter for a specific field based on it's key
add_filter('acf/fields/relationship/query/key=field_5aa2eb46c8942', 'related_relationship_query', 10, 3);

