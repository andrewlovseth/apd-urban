<?php

/*

	Template Name: Contact

*/

get_header(); ?>

	<?php get_template_part('partials/hero'); ?>

	<section id="main">
		<div class="wrapper">
			
			<div class="header center">
				<h1 class="page-header"><?php the_title(); ?></h1>
			</div>

			<section id="contact-info">
				
				<h4>Address</h4>
				<p><?php the_field('address', 'options'); ?></p>

				<h4>Phone</h4>
				<p><?php the_field('phone', 'options'); ?></p>

				<h4>Fax</h4>
				<p><?php the_field('fax', 'options'); ?></p>

				<h4>Email</h4>
				<p><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></p>

			</section>

			<?php if(have_rows('job_openings')): ?>

				<section id="job-openings">
					<div class="header center">
						<h3 class="section-header"><?php the_field('job_openings_headline'); ?></h3>
					</div>

					<?php while(have_rows('job_openings')): the_row(); ?>
					 
					 	<div class="job">
					  		<a href="<?php the_sub_field('pdf'); ?>" rel="external"><?php the_sub_field('title'); ?></a>
					 	</div>

					<?php endwhile; ?>
				</section>

			<?php endif; ?>


			<section id="contact-form">
				<div class="header center">
					<h3 class="section-header">Get in touch</h3>
				</div>

				<?php echo do_shortcode('[wpforms id="365" title="false" description="false"]'); ?>


			</section>

		</div>
	</section>

	<section id="map">
		<a href="<?php the_field('map_link'); ?>" rel="external" class="cover" style="background-image: url(<?php $image = get_field('map'); echo $image['url']; ?>);"></a>
	</section>

<?php get_footer(); ?>