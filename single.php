<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">

			<article>
				
				<div class="featured-image">
					<img src="<?php $image = get_field('featured_image'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
				</div>
				
				<div class="header center">
					<h4><?php the_time('F j, Y'); ?></h4>
					<h1 class="page-header"><?php the_title(); ?></h1>

					<?php $post_object = get_field('author'); if( $post_object ): $post = $post_object; setup_postdata( $post ); ?>
						<div class="author">

							<div class="photo">
								<a href="<?php the_permalink(); ?>">
									<img src="<?php $image = get_field('headshot'); echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
								</a>
							</div>

							<div class="info">
								<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
								<h6><?php the_field('title'); ?></h6>
							</div>
						</div>
					<?php wp_reset_postdata(); endif; ?>
					
				</div>

				<div class="body">

					<?php if ( have_posts() ): while ( have_posts() ): the_post(); ?>
						<?php the_content(); ?>
					<?php endwhile; endif; ?>

				</div>

			</article>
	
		</div>
	</section>

<?php get_footer(); ?>