<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">
			
			<div class="header center">
				<h1 class="page-header">Blog</h1>
			</div>

			<?php if ( have_posts() ): ?>

				<div class="posts">

					<?php while ( have_posts() ): the_post(); ?>

						<?php get_template_part('partials/blog-preview'); ?>

					<?php endwhile; ?>

				</div>

				<?php get_template_part('partials/pagination'); ?>

			<?php endif; ?>

		</div>
	</section>
	
<?php get_footer(); ?>