<div class="col col-3 contact-info">

	<div class="section-header-wrapper">
		<h3 class="section-header">Atlanta Office</h3>
	</div>

	<div class="sub-col">
		<h4>Address</h4>
		<p><?php the_field('address', 'options'); ?></p>
	</div>

	<div class="sub-col">
		<h4>Phone</h4>
		<p><?php the_field('phone', 'options'); ?></p>

		<h4>Fax</h4>
		<p><?php the_field('fax', 'options'); ?></p>
	</div>

	<div class="sub-col">
		<h4>Email</h4>
		<p><a href="mailto:<?php the_field('email', 'options'); ?>"><?php the_field('email', 'options'); ?></a></p>
	</div>

	<div class="social">
		<h4>Social</h4>
		<a href="<?php the_field('linkedin', 'options'); ?>" class="linkedin" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/linkedin.svg" alt="LinkedIn"></a>
		<a href="<?php the_field('facebook', 'options'); ?>" class="facebook" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/facebook.svg" alt="Facebook"></a>
		<a href="<?php the_field('instagram', 'options'); ?>" class="instagram" rel="external"><img src="<?php bloginfo('template_directory') ?>/images/instagram.svg" alt="Instagram"></a>
	</div>

</div>