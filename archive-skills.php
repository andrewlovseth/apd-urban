<?php get_header(); ?>

	<section id="main">
		<div class="wrapper">
			
			<div class="header center">
				<h1 class="page-header">Skills</h1>

				<h2 class="subheadline"><?php the_field('skills_subheadline', 'options'); ?></h2>

				<div class="copy">
					<?php the_field('skills_copy', 'options'); ?>
				</div>	
			</div>

			<?php
				$args = array(
					'post_type' => 'skills',
					'posts_per_page' => 150,
					'orderby' => 'title',
					'order' => 'ASC'
				);
				$skills = new WP_Query( $args );
				if ( $skills->have_posts() ) : ?>

					<section id="skill-list">
						<?php while ( $skills->have_posts() ) : $skills->the_post(); ?>

							<?php get_template_part('partials/skill'); ?>
						
						<?php endwhile; ?>
					</section>

			<?php endif; wp_reset_postdata(); ?>

		</div>
	</section>
	
<?php get_footer(); ?>