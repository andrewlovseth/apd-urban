<?php get_header(); ?>

	<section id="hero" class="cover" style="background-image: url(<?php $image = get_field('main_image'); echo $image['url']; ?>);">
		<div class="wrapper">


		</div>
	</section>

	<section id="main">
		<div class="wrapper">
			
			<div class="header center page">
				<h1 class="page-header"><?php the_title(); ?></h1>

				<?php if(get_field('subheadline')): ?>
					<h2 class="subheadline"><?php the_field('subheadline'); ?></h2>
				<?php endif; ?>

				<div class="copy">
					<?php the_field('description'); ?>
				</div>
			</div>


			<?php $ancestors = $post->ancestors; if($ancestors): ?>

				<div class="header center section">
					<h3 class="section-header">Projects</h3>
				</div>

				<?php
					$slug = $post->post_name;

					$args = array(
						'post_type' => 'projects',
						'posts_per_page' => 200,
						'order' => 'ASC',
						'orderby' => 'title',
						'meta_query' => array(
							array(
								'key' => 'services',
								'value' => '"' . get_the_ID() . '"',
								'compare' => 'LIKE'
							),
				            array(
				                'key' => '_wp_page_template',
				                'value' => 'case-study.php',
				                'compare' => '!='
				            ),
						),
					);
					$projects = new WP_Query( $args );
					if ( $projects->have_posts() ) : ?>

						<div class="project-list">
							<?php while ( $projects->have_posts() ) : $projects->the_post(); ?>
								<?php get_template_part('partials/project'); ?>
							<?php endwhile; ?>
						</div>

				<?php endif; wp_reset_postdata(); ?>

			<?php else: ?>

				<div class="header center section">
					<h3 class="section-header">Services</h3>
				</div>

				<?php
					$args = array(
						'post_type' => 'services',
						'posts_per_page' => 25,
						'post_parent' => $post->ID,
						'order' => 'ASC',
						'orderby' => 'menu_order',

					);
					$services_list = new WP_Query( $args );
					if($services_list->have_posts()) : ?>
					
						<div class="service-list">

							<?php while ( $services_list->have_posts() ) : $services_list->the_post(); ?>

								<div class="service">
									<div class="photo">
										<a href="<?php the_permalink(); ?>" class="cover" style="background-image: url(<?php $image = get_field('main_image'); echo $image['sizes']['medium']; ?>);">
										</a>
									</div>

									<div class="info">
										<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

										<div class="excerpt">
											<?php the_field('short_description'); ?>
										</div>

										<div class="read-more">
											<a href="<?php the_permalink(); ?>">Read more</a>

										</div>
									</div>


								</div>

							<?php endwhile; ?>
							
						</div>


				<?php endif; wp_reset_postdata(); ?>

			<?php endif; ?>



		</div>
	</section>

<?php get_footer(); ?>