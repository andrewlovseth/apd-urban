<div class="skill">
	<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>

	<div class="description">
		<?php the_content(); ?>
		<a href="<?php the_permalink(); ?>">View projects</a>
	</div>
</div>